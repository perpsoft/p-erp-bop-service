const {Pool} = require('pg');
//const log = require('../config/logger');
/**
 * @class Postgres
 * @name Modules.Postgres
 * @description Клас для підключення до бази данних системи Позначення
 * @memberOf Modules
*/
class Postgres {
  constructor(dbOptions) {
    this.temp = null;
    this.file = null;
    this.claim = null;

    this.tempClient = new Pool({database: process.env.PGDATABASE_TEMP, ...dbOptions});
    this.fileClient = new Pool({database: process.env.PGDATABASE_FILE, ...dbOptions});
    this.claimClient = new Pool({database: process.env.PGDATABASE_CLAIM, ...dbOptions});

    /*this.tempClient.on('error', (err, client) => {
      log.error({action: 'POSTGRES CONNECTION', error: 'Unexpected error on idle tempClient', err})
      console.log('Unexpected error on idle fileClient', err)
      //process.exit(-1)
    })
    this.fileClient.on('error', (err, client) => {
      log.error('Unexpected error on idle fileClient', err)
      process.exit(-1)
    })
    this.claimClient.on('error', (err, client) => {
      log.error('Unexpected error on idle claimClient', err)
      process.exit(-1)
    })*/
  }

  async connectionPending(){
    this.temp = await this.tempClient.connect();
    this.file = await this.fileClient.connect();
    this.claim = await this.claimClient.connect();
  }
}

const db = new Postgres({
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  password: process.env.PGPASSWORD,
  port: process.env.PGPORT,
})

module.exports = db;