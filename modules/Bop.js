global.XMLHttpRequest = require('xhr2')
const UB = require('@unitybase/ub-pub')
const log = require('../config/logger');

const HOST = process.env.UB_HOST || 'http://snake.ukrpatent.local'
const USER = process.env.UB_USER || 'admin'
const PWD = process.env.UB_PWD || 'admin'

class Bop{
  async connectionPending(){
    this.conn = await UB.connect({
      host: HOST,
      onCredentialRequired: function (conn, isRepeat) {
        if (isRepeat) {
          throw new UB.UBAbortError('invalid credential')
        } else {
          return Promise.resolve({authSchema: 'UB', login: USER, password: PWD})
        }
      },
      onAuthorizationFail: function (reason) {
        log.error({action: 'BOP_API_AUTH', error: reason});
      }
    })

    log.info({action: 'BOP_API_CONNECTED'});

/*  this.opertype = conn.Repository('up_opertype')
    this.operation = conn.Repository('up_operation')
    this.enclosure = conn.Repository('up_enclosure')
    this.invoiceitem = conn.Repository('up_invoiceitem') */
  }
}

module.exports = new Bop();