const fs = require('fs');
if (process.env.NODE_ENV && fs.existsSync(`./.env.${process.env.NODE_ENV.trim()}`)) {
  require('dotenv').config({path: `./.env.${process.env.NODE_ENV.trim()}`});
} else {
  require('dotenv').config();
}