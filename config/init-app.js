const log = require('./logger');
const Postgres = require('../modules/Postgres');
const Bop = require('../modules/Bop')


const initApp = async function () {
  try {

    await Postgres.connectionPending();
    
/*     await Bop.connectionPending();
    console.log(`Hello, ${Bop.conn.userLogin()}!`) */
    


    console.log('initApp', 'SERVICE STARTED')
    log.info({'action': 'SERVICE STARTED'})

  } catch (error) {
    console.log(error)
  }
};


module.exports = initApp;
