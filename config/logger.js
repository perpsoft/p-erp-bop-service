const winston = require('winston');
const ip = require("ip");
const config = require('../config');

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
};

const options = {
  file: {
    level: levels,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    colorize: false,
  },
  console: {
    level: levels,
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

const logger = new winston.createLogger({
  level: 'silly',
  format: winston.format.json(),
  defaultMeta: {
    "X-Service-Name": config.get('X-Service-Name'),
    "X-Service-Version": config.get('X-Service-Version'),
    "X-Service-IP": ip.address(),
    time: new Date().toUTCString()
  },
  transports: [
    new winston.transports.Console(options.console),
    new winston.transports.File({ filename: './logfiles/log.log' })
  ]
});

module.exports = logger;